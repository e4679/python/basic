menu = """
Bienvenido al conversor de monedas 😎 

1 - Peso Colombiana
2 - Peso Argentino
3 - Peso Mexicano

Elege una opciòn: """
cop_trm = 4450.34
ars_trm = 129.71
mxn_trm = 20.54

def conver_to_usd(currency, trm):
    amount = float(input('How much money do you have in '+ currency+ ': '))
    usd_amount = round(amount / trm, 2)
    print('You have ' + str(usd_amount) + ' USD')


def start():
    opcion = int(input(menu))


    if opcion == 1:
        conver_to_usd('COP', cop_trm)
    elif opcion == 2:
        conver_to_usd('ARS', ars_trm)
    elif opcion == 3:
       conver_to_usd('MXN', mxn_trm)
    else:
        print('Ingresa una opcion correcta por favor')
        print('');
        start()

start()