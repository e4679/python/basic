menu = """
Bienvenido al conversor de monedas 😎 

1 - Peso Colombiana
2 - Peso Argentino
3 - Peso Mexicano

Elege una opciòn: """

opcion = int(input(menu))
usd_trm_to_cop = 4450.34
usd_trm_to_ars = 129.71
usd_trm_to_mxn = 20.54

if opcion == 1:
    cop_amount = float(input('¿How much money do you have in COP? '))
    usd_amount = round(cop_amount / usd_trm_to_cop, 2)
    print('You have ' + str(usd_amount) + ' USD')
elif opcion == 2:
    cop_amount = float(input('¿How much money do you have in ARS? '))
    usd_amount = round(cop_amount / usd_trm_to_ars, 2)
    print('You have ' + str(usd_amount) + ' USD')
elif opcion == 3:
    cop_amount = float(input('¿How much money do you have in MXN? '))
    usd_amount = round(cop_amount / usd_trm_to_mxn, 2)
    print('You have ' + str(usd_amount) + ' USD')
else:
    print('Ingresa una opcion correcta por favor')
